var namesArr = Array();

function addStudent(name){
    namesArr.push(name);
    return console.log(`${name} was added to the student's list`)
}

function countStudents(){
    var studCount = namesArr.length;
    console.log("There are total of ", studCount, " students enrolled.");
}

function printStudents(){
    namesArr.forEach((name, index) =>{
        console.log(name)
    })
}

function findStudent(name){
    if(namesArr.includes(name)){
        console.log(name + "is an Enrollee");
    } else {
        console.log("No student found with the name of", name);
    }
}

function removeStudent(name){
    var remName = namesArr.findIndex(checkName);
    function checkName(nameofElem){
        return nameofElem === name;
    }
    namesArr.splice(remName, 1);
    console.log(name + "was removed from the student's list");
}
