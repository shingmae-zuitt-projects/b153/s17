/*
ARRAYS AND INDICES (plural for index)

index - number assigned from 0

Arrays are used to store multiple values in a single variable.

Arrays are declared using square brackets [], also known as ARRAY LITERALS.

Values inside of an array are called "elements".
    -always separated by a comma (,)

*/



let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu']

let tasks = [
    'shower',
    'eat breakfast',
    'go to work',
    'go home',
    'go to sleep'
]


let emptyArr = []

let example1 = ["cat", "cat", "cat"]
example1[2] = "ShingShing"
console.log(example1);

console.log(computerBrands[3]);


/*

READING FROM/ACCESSING ELEMENTS INSIDE AN ARRAY

To access an array element, simply denote the element's index number inside a pair of square brackets next to the name/ variable of the array holding it

to get an element's index, simply count starting at zero from the first of an array

If indexOf cannot find your specified element inside of an array, it returns -1

*/

/*
if(emails.indexOf("shing@mail.com") === -1){
    //user can register, because it did not find any duplication
    console.log("Congrats! you are now registered")
}
*/

/*

ARRAY METHODS:
(Method is just another word for fuunction)

.push() - adds an element at the end of an array AND returns the array's length

.pop() - removes an array's LAST element AND returns the removed element
*/

console.clear();
let grades = [98.5, 94.3, 89.2, 90.1]
grades[3] = 80.1
console.log(grades)
console.log(grades.length)
console.log(grades.indexOf(70))

grades[4] = 99
console.log(grades)

grades[grades.length] = 100
console.log(grades)

grades.push(88.8)
console.log(grades)

grades.pop()
console.log(grades)

console.log(grades.pop())

grades.push(grades.pop())
console.log(grades)

let classA = ["John", "Jacob", "Jack"]

let classB = ["Bob", "Bill", "Ben"]

classB.push(classA.pop())
console.log(classB)


//Adds another element at the first part

grades.unshift(77.7)
console.log(grades)

grades.shift()

console.log(grades)
console.log(grades.shift())

//.splice() - can add and remove from anywhere in an array, and even do both at once

console.clear()
console.log(tasks);

//syntax: .splice(starting index number, number of items to remove, items to add)

//ADD WITH SPLICE
tasks.splice(3, 0, "eat lunch")

tasks.splice(2,1)

tasks.splice(3,2,"code", "code some more")
console.log(tasks)


console.clear()
//.sort() - rearranges array elements in alphanumeric order

computerBrands.sort()
console.log(computerBrands)

//.reverse() - reverse the array order
computerBrands.reverse()
console.log(computerBrands)

console.clear()
//.concat() - adds at the end of the first array
console.log("herrree")
let a = [1, 2, 3, 4, 5]
let b = [6, 7, 8, 9, 10]

let ab = a.concat(b)
console.log(ab)


//.join() - converts the elements of an array into a new string string
let joinedArray = ab.join("")
console.log(joinedArray)


let arr1 = [1, 2, 3]
let arr2 = [1, 2, 3]

console.log(arr1.join() === arr2.join())

//.slice() - copies specified array elements into a new array

//syntax - slice(starting array, ending array)

let c = a.slice(3,4)
console.log(a)
console.log(c)

let animal = "hippopotamus"
console.log(animal[0])
animal.slice(2,5)
console.log(animal)




console.clear()
//Mini Activity
let numbersArr = [1,2,3,4,5,6,7,8,9,10]
let placeholderArr = []

for(let i = 0; i < numbersArr.length; i++){
    if(
        numbersArr[i] % 2 === 1
    ){
        continue;
    }else{
        placeholderArr += numbersArr[i]
    }
}
console.log(placeholderArr);


//Sir's solution:
let numbersArr2 = [1,2,3,4,5,6,7,8,9,10]
let placeholderArr2 = []

for(let i = 0; i < numbersArr2.length; i++){
    if(
        numbersArr2[i] % 2 === 0
    ){
        placeholderArr2.push(numbersArr2[i])
    }
}
console.log(placeholderArr2)


//ARRAY ITERATION METHODS
//Javascript loops only for arrays
//Always loops through the array the exact number of times as there are elements in the array

//.forEach()

//brand is a placeholder

computerBrands.forEach(function(brand){
    console.log(brand)
})

//Works exactly the same as:
for(let i = 0; i < computerBrands.length; i++){
    console.log(computerBrands[i])
}

//.map() - creates new array populated with the results of running a function on each element of the mapped array. Can't continue inside map.

let numbersArr4 = [1,2,3,4,5,6,7,8,9,10]

let doubledNumbers = numbersArr4.map(function(number){
    return number * 2
})

console.log(doubledNumbers)

//.every() - check every element in an array and see if the ALL match your given condition

let allValid = numbersArr4.every(function(number){
    return number < 10.1
})

console.log(allValid)

//.some()

let someValid = numbersArr4.some(function(number){
    return number < 2
})

console.log(someValid)

//.filter() - creates a new array only populated with elements match our given condition

let words = ['spray', 'limit', 'elite', 'exuberant', 'destruction', 'present']

let wordsResult = words.filter(function(word){
    return word.length > 5;
})

console.log(wordsResult)

//.includes() - checks if the value is included in the array (returns true or false)

console.log(words.includes('limit'))

let twoDimensional = [
    [1.1, 1.2, 1.3],
    [2.1, 2.2, 2.3],
    [3.1, 3.2, 3.3]
]

console.log(twoDimensional[0][1])